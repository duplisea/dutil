plotDaniel = function(x0,y0,Lx,Ly, proj)
{
	X = lapply(x0, function(x){ c(x+0.5*Lx, x+0.5*Lx, x-0.5*Lx, x-0.5*Lx) })
	Y = lapply(y0, function(y){ c(y+0.5*Ly, y-0.5*Ly, y-0.5*Ly, y+0.5*Ly) })

	centroid = as.EventData(data.frame(EID=1:length(x0), X=x0, Y=y0), projection=proj)
	corners  = as.PolySet(data.frame(PID=rep(1:length(x0),each=4), POS=rep(1:4, length(x0)), X=unlist(X), Y=unlist(Y)), projection=proj)

	plotPolys(corners, type="n", xlim=extendrange(X), ylim=extendrange(Y), projection=proj, plt=NULL)
	addPoints(centroid, pch=19, col="red")
	addPolys(corners, col="transparent", border="gainsboro")
	points(corners$X, corners$Y, pch=19, col="blue")
}


library(dutil)
library(PBSmapping)
## simple
plotDaniel(x0=10, y0=10, Lx=2.5, Ly=2.5, proj=1)
## HS grid
data("hsgrid", package="PBSdata")
hscent = calcCentroid(hsgrid)
plotDaniel( x0=hscent$X, y0=hscent$Y, Lx=diff(sort(unique(hsgrid$X)))[1], Ly=diff(sort(unique(hsgrid$Y)))[1], proj=attributes(hsgrid)$projection )
