## What is it

“A database is presented that describes the pelagic and benthic habitats
of the 0–30 m surface layer in the estuary and Gulf of St. Lawrence. A
grid made of 6.25 km² cells (2.5 x 2.5 km) was used to aggregate the
data. Each of the 39,337 cells overlapping the marine environment was
characterized using landscape, hydrographic, and oceanographic
parameters determined from observations (including satellite imagery), a
3D circulation model, and published and unpublished material available
at local and regional scales. The low tide limit was taken as the upper
(0 m) boundary, but neighbouring landscape features, such as the
proximity to freshwater inflows, surface area of the tidal zone, and
characteristics of the shoreline, were also taken into consideration.
The dataset includes 130 descriptors: cell location parameters such as
cell address, latitude and longitude, and distance to the coast;
landscape features such as depth, slope, insulosity, coastline
development, shore material and characteristics, degree of protection
from the open sea, sensitivity of the shoreline to sea-level rise, and
relative importance of the benthic and pelagic environments;
hydrographic and oceanographic parameters such as distance to the
nearest stream or river and its drainage area and mean annual flow,
tidal range, vertical and horizontal currents, sea-surface climatology,
ice conditions, salinity and temperature at various depths, stability of
the water column. A total of 103 variables were used to classify cells
into 14 different habitats. Coastal areas, particularly in the southern
Gulf, appear to be more diversified locally than midshore and offshore
habitats, which formed large patches of more uniform characteristics.
The dataset provides useful information on the spatial extent of major
coastal epipelagic habitats in the study area. The information can be
used for mapping purposes and for analyses of species-habitat
relationships, a key requirement for conservation, integrated
management, or species-at-risk recovery planning purposes.” (Abstract
direct from Dutil et al. 2011)

<figure>
<img src="./README_files/figure-markdown_strict/dutil.gsl.jpg"
alt="Gulf of St. Lawrence, Canada. From Dutil et al 2013" />
<figcaption aria-hidden="true">Gulf of St. Lawrence, Canada. From Dutil
et al 2013</figcaption>
</figure>

      map.f()
      points(dutil$LONGITUDE, dutil$LATITUDE, pch=".", col=5*(dutil$BSAL_MEAN), cex=0.1)
      title("Bottom water salinity, raw data")

![](README_files/figure-markdown_strict/salinity.raw-1.png)

      tmp= dutil[,c(1,2,5,6,61)] # create a subset
      tgam= gam(BSAL_MAX~ te(LONGITUDE, LATITUDE), data=tmp)
      tmp$smooth= predict(tgam)
      tmp$smooth[tmp$smooth<0]=0
      map.f()
      points(tmp$LONGITUDE,tmp$LATITUDE,col=tmp$smooth, cex=2, pch=".")
      title("Bottom water salinity, GAM smoothed")

![](README_files/figure-markdown_strict/salinity.smooth-1.png) Be
careful with the smoothing. It is creating a two way gam between
positions for a variable and therefore it does not account for
land-masses. Smoothing is probably ok for open areas of sea but will
smooth too much and likely incorrectly for inlets and complex
coastlines.

## References

Dutil, J.-D., S. Proulx, P. S. Galbraith, J. Chassé, N. Lambert, and C.
Laurian. 2012. Coastal and epipelagic habitats of the estuary and Gulf
of St. Lawrence. Can. Tech. Rep. Fish. Aquat. Sci. 3009: ix + 87
pp. <https://publications.gc.ca/collections/collection_2012/mpo-dfo/Fs97-6-3009-eng.pdf>

Dutil, J.-D., S. Proulx, P.-M. Chouinard, D. Borcard, C. Laurian, H.
Tamdrari, and C. Nozères. 2013. A standardized database to describe and
classify offshore benthic marine habitats and its use for designating
the critical habitat of species at risk. Can. Manuscr. Rep. Fish. Aquat.
Sci 3014: vi + 347 pp.
